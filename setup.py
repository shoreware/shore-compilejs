import os
from setuptools import setup


setup(
    name='shore-compilejs',
    version='0.2.0',
    description="Convert PO files to JS",
    keywords='PO i18n internationalisation JSON gettext shore',
    author='Sebasien Fievet',
    author_email='sebastien@shore.li',
    license='BSD',
    url='https://bitbucket.org/shoreware/shore-compilejs',
    install_requires=[
        'babel',
    ],
    scripts=['bin/shore-compilejs'],
)
