shore-compilejs
===============

``shore-compilejs`` is a command line tool that aims to compile PO files
into a Shoreware ad-hoc Javascript catalog file, leveraging `babel`_ to
compile PO files to JSON.

.. _babel: http://babel.pocoo.org/
